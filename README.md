# Enqueue

Enqueue is a command-line tool, written in Python 3, for downloading podcasts
and (if you have access to it) BBC iPlayer radio content. As the name suggests,
it is based on the idea of *queues*: you listen to the files at the start of the
queue then delete them, while Enqueue adds new ones at the end. If you manage it
right, you can sustain a constant stream of things to listen to.

There are actually two queues supported:

  - a *files* queue, intended for podcasts, where each file is added
    individually;

  - a *directories* queue, intended for audiobooks and radio series, where files
    are grouped into directories before being added.

In both cases, the items added to the queue are renamed so that if you listen to
them in alphabetical order they play chronologically, earliest first. (I use
[Music Folder Player] for this.)

[Music Folder Player]: http://zorillasoft.de/MusicFolderPlayer.html


## Installation

Before using Enqueue, you need to install the following:

  - [Python] (version 3)
  - Additional Python libraries [Requests], [Dateutil], [Clint] and [defusedxml]
  - [get-iplayer] (if you want to download from BBC iPlayer)
  - [aacgain]
  - [vorbisgain]

[Python]: https://www.python.org
[Requests]: https://pypi.org/project/requests/
[Dateutil]: https://pypi.org/project/python-dateutil/
[Clint]: https://pypi.org/project/clint/
[defusedxml]: https://pypi.org/project/defusedxml/
[get-iplayer]: https://github.com/get-iplayer/get_iplayer
[aacgain]: http://aacgain.altosdesign.com/
[vorbisgain]: https://www.sjeng.org/vorbisgain.html

Having done all that, you can simply download the `nq` script and run it.
Depending on your OS, Python installation and where you put it, you may need to
run it in one of the following ways:

    nq
    ./nq
    python nq
    python3 nq

The first time you run it, you will be asked what directories to use for the two
queues. These directories must already exist and be writeable: Enqueue will not
create them for you.

The directory you choose for the files (podcast) queue will be used directly to
store the downloaded files.

The directory you choose for the directories (audiobook) queue will actually
contain two subdirectories: `Working` and `Queued`. Episodes will be downloaded
to directories in `Working` in the first instance. When those directories reach
a certain quota (that you have to set), they are moved to `Queued` and renamed.

If you listen to your audio files on a difference device from the one where you
do the downloading, I recommend using something like [Syncthing] to sync your
podcast queue and `Queued` folder across the two machines.

[Syncthing]: https://syncthing.net/


## Information-only commands

### The basics

These commands prevent any further operations being executed.

Display help and exit:

    nq -h
    nq --help

Display more specific help and exit:

    nq files -h
    nq dirs -h
    nq quotas -h
    nq show -h

Display version and exit:

    nq -v
    nq --version


### Inspecting the config file

To show all the configuration settings:

    nq show

To show a specific section, you can specify an option, like so:

    nq show --queues

The available options are these:

  - `-q`/`--queues`: directories holding the queues.

  - `-n`/`--quotas`: quotas for directories of episodes.

  - `-f`/`--files`: subscriptions for the files queue.

  - `-d`/`--dirs`: searches saved for the directories queue.

  - `-u`/`--feeds`: feed nicknames and URLs across both queues.

  - `-s`/`--series`: BBC iPlayer programme names across both queues.

  - `-e`/`--episodes`: BBC iPlayer episode names across both queues.

  - `-i`/`--pids`: BBC iPlayer PIDs across both queues.

You can specify multiple options at once; the result is additive.

If you want to stop subscribing to or searching for a particular bit of content,
this command is useful for looking up the precise string to remove.


## Downloading content

The basic commands for downloading content are as follows.

  - Download into both queues:

        nq

  - Download into the file (podcast) queue only:

        nq files

  - Download into the directory (audiobook) queue only:

        nq dirs

These will use subscriptions/searches saved to the config file to find suitable
content to download.

By default, files will not be added to the files queue if they would appear
earlier than the current earliest file. If the queue is empty, only files
published in the last week are downloaded. You can override this with an option,
specifying the date in yyyy-mm-dd format:

    nq -d <date>
    nq --date <date>
    nq files -d <date>
    nq files --date <date>

This option is silently ignored by `nq dirs`, since typically if you are
downloading an audiobook you will want it from the beginning. If, however, you
want to jump in at a particular point, you can:

    nq dirs -c <date>
    nq dirs --since <date>


### Downloading specific content to a specific queue

If you specify a queue, then the following search options become available. They
allow you to bypass the config file and download specific content directly.

  - `-u`/`--url`: The URL of a podcast feed.

  - `-f`/`--feed`: The nickname for a podcast feed. The script will look for a
    corresponding URL on the command line or, failing that, the config file. If
    no URL is found the script will exit with an error. When choosing a
    nickname, you will get best results if you stick to safe characters
    (letters, numbers, hyphen, underscore, tilde) as it is used in path names.

  - `-s`/`--series`: The name of a programme on the BBC iPlayer.

  - `-e`/`--episode`: The name of an episode on the BBC iPlayer.

  - `-i`/`--pid`: The PID of a programme or episode on the BBC iPlayer (usually
    begins with `b`). You can also give the URL of the corresponding page as
    Enqueue will automatically remove the `https://www.bbc.co.uk/programmes/`
    bit for you.

The feed options (`-f`, `-u`) can only be specified once. The others can be
specified multiple times.

Examples:

    nq files -u "https://great-podcast.net/feed" -f "Great-Podcast"
    nq dirs -s "Doctor Who" -s "Bleak Expectations"


### Updating the config file

If you specify a queue and one or more of the above search options, then three
more options become available:

  - `-r`/`--repeat`: Perform the download now and also save the
    subscription/search to the config file.

  - `-l`/`--later`: Do not perform the download now, but save the
    subscription/search to the config file.

  - `-n`/`--never`: Do not perform the download now, and remove the
    subscription/search from the config file.

Example:

    nq files --url <url> --feed <nickname> --repeat


### Changing the queue directory

You can temporarily or persistently change the directories used for enqueuing
content:

  - `-t`/`--to`: The directory in which to enqueue the files or (sub)directories
    on this and all future occasions.

  - `-o`/`--once-to`: The directory in which to enqueue the files or
    (sub)directories on just this occasion.


## Moving working directories to the queue

As mentioned above, episodes downloaded to the directories queue are first kept
in the `Working` area. Only once the directory is ‘full’ is it renamed with a
date prefix and put in the `Queued` area.

To set the quota for all new directories:

    nq quotas

To change quotas that have been set previously:

    nq quotas -r
    nq quotas --reset

You don't have to have all the episodes in a single queued directory. If, for
example, you are downloading a twelve-part series, you could set a quota of
`4,4,4` to split them into three separately queued directories. After the first
four episodes have been downloaded, they will be moved to an appropriately named
folder in the `Queued` directory; the working directory will be removed (if
empty); and the quota will be reset to `4,4`.

If the next group never arrives, you might end up with quotas set for a
non-existent working directory. If you want to clear the quota from your config
file, you can do so with the following command:

    nq quotas -c
    nq quotas -clean

More specifically, what it does is delete the quota for any directory that *is*
listed in the config file but *is not* present in the `Working` directory.

## Historical note

This script is my third attempt at this sort of thing. The immediately prior
version was [get-audio](https://github.com/alex-ball/utility-scripts).
